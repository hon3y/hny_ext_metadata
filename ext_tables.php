<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_metadata', 'Configuration/TypoScript', 'hive_ext_metadata');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextmetadata_domain_model_metadata', 'EXT:hive_ext_metadata/Resources/Private/Language/locallang_csh_tx_hiveextmetadata_domain_model_metadata.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextmetadata_domain_model_metadata');

    }
);
